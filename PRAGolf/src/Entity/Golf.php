<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GolfRepository")
 */
class Golf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localisation;

    /**
     * @ORM\Column(type="integer")
     */
    private $par;

    /**
     * @ORM\Column(type="integer")
     */
    private $numeroTrou;

    /**
     * @ORM\Column(type="integer")
     */
    private $tempsDeplacement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getPar(): ?int
    {
        return $this->par;
    }

    public function setPar(int $par): self
    {
        $this->par = $par;

        return $this;
    }

    public function getNumeroTrou(): ?int
    {
        return $this->numeroTrou;
    }

    public function setNumeroTrou(int $numeroTrou): self
    {
        $this->numeroTrou = $numeroTrou;

        return $this;
    }

    public function getTempsDeplacement(): ?int
    {
        return $this->tempsDeplacement;
    }

    public function setTempsDeplacement(int $tempsDeplacement): self
    {
        $this->tempsDeplacement = $tempsDeplacement;

        return $this;
    }
}
