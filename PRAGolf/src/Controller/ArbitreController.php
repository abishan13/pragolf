<?php

namespace App\Controller;

require __DIR__.'../../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

// Include PhpSpreadsheet required namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ArbitreController extends Controller
{
    /**
     * @Route("/arbitre", name="arbitre")
     */


    public function index()
    {
        return $this->render('arbitre/index.html.twig', [
            'controller_name' => 'Site GoodMovie',
        ]);

    }

    /**
     * @Route("/arbitre/create", name="create_xlsx")
     */
    public function creationXlsx()
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        $sheet->setTitle("My First Worksheet");

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = 'my_first_excel_symfony4.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }


    /**
     * @Route("/arbitre/read", name="read_xlsx")
     */
    public function readXlsx()
    {
        $tmpfname = "uploads/my_first_excel_symfony4.xlsx";
        $excelReader = IOFactory::createReaderForFile($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        echo "<table>";
        for ($row = 1; $row <= $lastRow; $row++) {
            echo "<tr><td>";
            echo $worksheet->getCell('A'.$row)->getValue();
            echo "</td><td>";
            echo $worksheet->getCell('B'.$row)->getValue();
            echo "</td><tr>";
        }
        return new Response($tmpfname);
    }


}
