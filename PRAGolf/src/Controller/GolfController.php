<?php
namespace App\Controller;
use App\Entity\Golf;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
class GolfController extends Controller
{
 /**
  * @Route("/golf", name="golf")
  */
    public function index()
    {
        $golf = $this->getDoctrine()
            ->getRepository(Golf::class)
            ->findAll();
        return $this->render('golf/index.html.twig', compact('golf'));
    }

    /**
     * @Route("/golf/ajout", name="ajout_golf")
     */
    public function ajoutGolf(Request $request)
    {
        $item = new Golf();
        $item->setNom('');
        $item->setLocalisation('');
        $item->getPar('');
        $item->getNumeroTrou('');
        $item->getTempsDeplacement('');
        $form = $this->createFormBuilder($item)
            ->add('nom', TextType::class)
            ->add('localisation', TextType::class)
            ->add('par', TextType::class)
            ->add('NumeroTrou', TextType::class)
            ->add('TempsDeplacement', TextType::class)
            ->getForm();
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute('golf');
            }
        }
        return $this->render('golf/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}


